# Python exchange API wrapper ([changenow.io](https://changenow.io/))

## Requirements

Python 3.0 or greater

## API Key

To access the ChangeNOW API you need to generate an API key. You can get one in [your personal affiliate account](https://changenow.io/affiliate) or by emailing us at [api@changenow.io](mailto:api@changenow.io).

## Usage example
```python
from changenow_api.client import api_wrapper
from .changenow_api.exceptions import ChangeNowApiError

try:
    response = api_wrapper('CURRENCIES', fixed_rate=True)
except ChangeNowApiError as err:
    print(err)
```

## Supported calls and parameters
**Bold expected results require valid API Key**

Expected result	 | Calls and parameters
------------- | -------------
List of available currencies  | *call_name:* ```CURRENCIES```<br/><br/> *kwargs:*<br/> **active** - (Optional) Set true to return only active currencies (It's used for Standard flow). <br/>**fixed_rate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
List of available currencies for a specific currency | *call_name:* ```CURRENCIES_TO``` <br/><br/> *kwargs:*<br/>  **ticker** - (Required) Currency ticker.<br/>**fixed_rate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
Currency info | *call_name:* ```CURRENCY_INFO``` <br/><br/> *kwargs:*<br/>  **ticker** - (Required) Currency ticker.
**List of transactions** | *call_name:* ```LIST_OF_TRANSACTIONS``` <br/><br/> *kwargs:*<br/>**api_key** - (Required) Partner public API key<br/> **from_ticker** - (Optional) Set a ticker of a paying currency to filter transactions<br/>**to_ticker** - (Optional) Set a ticker of a payout currency to filter transactions<br/>**status** - (Optional) Set a transaction status (available statuses below) to filter transactions<br/>**limit** - (Optional) Limit of transactions to return (default: 10)<br/>**offset** - (Optional) Number of transactions to skip (default: 0)<br/>**date_from** - (Optional) Sort transactions by the date of creation. This request returns all transactions created after the specified date. Format: YYYY-MM-DDTHH:mm:ss.sssZ<br/>**date_to** - (Optional) Sort transactions by the date of creation. This request returns all transactions created before the specified date. Format: YYYY-MM-DDTHH:mm:ss.sssZ
**Transaction status** |  *call_name:* ```TX_STATUS``` <br/><br/> *kwargs:*<br/>**api_key** - (Required) Partner public API key<br/>  **id** - (Required) Transaction ID from Create transaction request
**Estimated exchange amount** | *call_name:* ```ESTIMATED``` <br/><br/> *kwargs:*<br/>**api_key** - (Required) Partner public API key<br/> **amount** - Amount of funds you want to exchange. <br/>**from_ticker** - (Required) Ticker of the currency you want to send<br/> **to_ticker** - (Required) Ticker of the currency you want to receive<br/>**fixed_rate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow
**Create exchange transaction** | *call_name:*  ```CREATE_TX``` <br/><br/> *kwargs:*<br/>**api_key** - (Required) Partner public API key<br/> **from_ticker** - (Required) Ticker of a currency you want to send.<br/>**to_ticker** - (Required) Ticker of a currency you want to receive.<br/>**address** - (Required) Address to receive a currency.<br/>**amount** - (Required) Amount you want to exchange.<br/>**fixed_rate** - (Optional) Set true to return only for the currencies available on a fixed-rate flow<br/>**extra_id** - (Optional) Extra ID for currencies that require it.<br/>**refund_address** - (Optional) Refund address.<br/>**refund_extra_id** - (Optional) Refund Extra ID.<br/>**user_id** - (Optional) Partner user ID.<br/>**payload** - (Optional) Object that can contain up to 5 arbitrary fields up to 64 characters long.<br/>**contact_email** - (Optional) Your contact email for notification in case something goes wrong with your exchange
List of all available pairs (Standard Flow) | *call_name:*  ```PAIRS``` <br/><br/> *kwargs:*<br/> **include_partners** - Set false to return all available pairs, except pairs supported by our partners.
List of available fixed-rate markets | *call_name:*  ```FIXED_RATE_PAIRS``` <br/><br/>*kwargs:*<br/>**api_key** - (Required) Partner public API key<br/>
Minimal exchange amount | *call_name:*  ```MIN_AMOUNT``` <br/><br/> *kwargs:*<br/> **from_ticker** - Ticker of the currency you want to send.<br/>**to_ticker** - Ticker of the currency you want to receive.<br/>

All methods described here ([API Documentation](https://changenow.io/api/docs)).

## Any questions
If you would like to contribute code you can do so through GitHub by forking the repository and sending a pull request.

If you have any question create an issue.

## License
Library is licensed under the [GPL-3.0 License](LICENSE).

Enjoy!
